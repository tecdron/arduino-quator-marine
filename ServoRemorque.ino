/*
 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/

#include <Servo.h>

Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo

int AngleON1 = 117;
int AngleOFF1 = 102;
int AngleON2 = 75;
int AngleOFF2 = 90;

int inPin1 = 3;   // contact sec pour servo 1
int inPin2 = 4;   // contact sec pour servo 2
int val1 = 0;     // variable to store the read value
int val2 = 0;     // variable to store the read value

void setup() 
{
  myservo1.attach(6);  // attaches the servo on pin 6 to the servo object
  myservo2.attach(10);  // attaches the servo on pin 10 to the servo object
  pinMode(inPin1, INPUT);      // sets the digital pin 3 as input
  pinMode(inPin2, INPUT);      // sets the digital pin 4 as input
}

void loop() 
{
  
  val1 = digitalRead(inPin1);   // read the input pin
  val2 = digitalRead(inPin2);   // read the input pin
  
  if (val1 == HIGH)
  {
    myservo1.write(AngleON1);
    //delay(500);   
  }
  else
  {
    myservo1.write(AngleOFF1);
    //delay(500);   
  }

  if (val2 == HIGH)
  {
    myservo2.write(AngleON2);
    //delay(500);   
  }
  else
  {
    myservo2.write(AngleOFF2);
    //delay(500);   
  }



}



